<?php

namespace Restful;

use Object;

/**
 * @author Neder Alfonso Fandi�o Andrade <neder.fandino@decameron.com>
 */
class Response {

    private $content;
    private $status;
    private $headers;
    private $buffer;
    
    /**
     * @var Object
     */
    private $hateoas;

    /**
     * @param mixed $content
     * @param int $status
     * @param array $headers
     */
    public function __construct($content = NULL, $status = NULL, $headers = []) {
        $this->buffer = ob_get_clean();
        $this->setContent($content);
        $this->status = $status;
        $this->headers = $headers;
    }

    /**
     * @param string $key
     * @param string $value
     * @param boolean $replace
     * @param int $status
     * @return static
     */
    public function header($key, $value) {
        $this->headers[$key] = $value;
        return $this;
    }

    /**
     * @param array $headers
     * @return static
     */
    public function withHeaders($headers) {
        foreach ($headers as $key => $value) {
            $this->header($key, $value);
        }
        return $this;
    }

    /**
     * @param int $status
     * @return static
     */
    public function setStatus($status) {
        if ($status) {
            $this->status = $status;
        }
        return $this;
    }

    /**
     * @param mixed $content
     * @return static
     */
    public function setContent($content) {
        $this->content = $content;
        return $this;
    }

    public function getContent() {
        if ($this->hateoas) {
            $response = new Object([
                'data' => $this->content
            ]);
            array_walk($this->hateoas->toArray(), function($value, $key) use (&$response) {
                $response->$key = $value;
            });
        } else {
            $response = $this->content;
        }
        return $response;
    }

    /**
     * @param mixed $content
     * @param int $status
     * @param array $headers
     * @return static
     */
    public function json($content, $status = NULL, $headers = []) {
        return $this->setContent($content)
                        ->setStatus($status)
                        ->withHeaders($headers)
                        ->header('Content-Type', 'application/json');
    }

    public function output() {
        $this->prepareHeaders();
        if (Header::isJsonContentTypeResponse()) {
            echo \Helpers\Json::encode($this->getContent());
        } else {
            $this->json($this->content, $this->status, $this->headers)
                    ->output();
        }
    }

    public function prepareHeaders() {
        Header::set($this->headers);
        http_response_code($this->status);
    }

    /**
     * @param callable $callback
     * @param array $userdata
     * @return $this
     */
    public function hateoas($callback, $userdata = []) {
        $hateoas = Input::hateoas();
        if ($hateoas) {
            $this->hateoas = new Object();
            array_unshift($userdata, $hateoas);
            array_unshift($userdata, $this->hateoas);
            call_user_func_array($callback, $userdata);            
        }
        return $this;
    }

}
