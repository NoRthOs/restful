<?php

namespace Restful;

use Exception as ExceptionBase;

/**
 * @author Neder Alfonso Fandi�o Andrade <neder.fandino@decameron.com>
 */
Trait Restful {

    /**
     * @param mixed $id
     */
    public function index($id = NULL) {
        if (Header::isPostHttpMethod() && is_null($id)) {
            $response = $this->store();
        } elseif (Header::isPutHttpMethod() && $id) {
            $response = $this->update($id);
        } elseif (Header::isPutHttpMethod()) {
            $this->outputBadRequest();
        } elseif (Header::isPatchHttpMethod() && $id) {
            $response = $this->amend($id);
        } elseif (Header::isPatchHttpMethod()) {
            $this->outputBadRequest();
        } elseif (Header::isDeleteHttpMethod() && $id) {
            $response = $this->destroy($id);
        } elseif (Header::isDeleteHttpMethod()) {
            $response = $this->outputBadRequest();
        } elseif (Header::isGetHttpMethod() && $id) {
            $response = $this->show($id);
        } elseif (Header::isGetHttpMethod()) {
            $response = $this->showAll();
        } else {
            $response = $this->outputMethodNotAllowed();
        }
        return $response;
    }

    protected function store() {
        $this->outputMethodNotAllowed();
    }

    protected function update($id) {
        $this->outputMethodNotAllowed($id);
    }

    protected function amend($id) {
        $this->outputMethodNotAllowed($id);
    }

    protected function destroy($id) {
        $this->outputMethodNotAllowed($id);
    }

    protected function show($id) {
        $this->outputMethodNotAllowed($id);
    }

    protected function showAll() {
        $this->outputMethodNotAllowed();
    }

    protected function outputMethodNotAllowed() {
        http_response_code(405);
    }

    protected function outputBadRequest() {
        http_response_code(400);
    }

    protected function isEmbed($resource) {
        $embed = Input::get('_embed');
        $pieces = explode(',', $embed);
        return in_array($resource, $pieces);
    }

    protected function call($method, $arguments = []) {
        $response = NULL;
        try {
            $methodExist = method_exists($this, $method);
            if ($methodExist && $this->isMethodAccessForCurrentHttpMethod($method)) {
                $response = call_user_func_array([$this, $method], $arguments);
            } else {
                $this->outputMethodNotAllowed();
            }
        } catch (ExceptionBase $exception) {
            $response = $this->handlerException($exception);
        }
        return $response;
    }

    protected function handlerException($exception) {
        throw $exception;
    }

    private function isMethodAccessForCurrentHttpMethod($method) {
        $metaData = $this->getMetaData($method);
        $httpMethod = Header::getHttpMethod();
        return empty($metaData['@HttpMethod']) ||
                in_array($httpMethod, $metaData['@HttpMethod']);
    }

    private function getMetaData($method) {
        $reflection = new \ReflectionMethod($this, $method);
        $comment = $reflection->getDocComment();
        $pattern = "#(@[a-zA-Z]+)\s+([a-zA-Z0-9, ()_].*)#";
        $matches = [];
        $counter = preg_match_all($pattern, $comment, $matches);
        $metaData = [];
        for ($index = 0; $index < $counter; $index++) {
            $metaData[$matches[1][$index]][] = trim($matches[2][$index]);
        }
        return $metaData;
    }

}
