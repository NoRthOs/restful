<?php

namespace Restful;

/**
 * @author Neder Alfonso Fandi�o Andrade <neder.fandino@decameron.com>
 */
class Header {

    protected static $httpMethods = [
        'GET',
        'POST',
        'PUT',
        'DELETE',
    ];

    public static function isJsonContentTypeRequest() {
        $header = filter_input(INPUT_SERVER, 'CONTENT_TYPE');
        return strpos($header, 'json') !== FALSE;
    }

    public static function isJsonContentTypeResponse() {
        return static::getResponse('Content-Type');
    }

    public static function isFormUrlencodedContentTypeRequest() {
        $header = filter_input(INPUT_SERVER, 'CONTENT_TYPE');
        return strpos($header, 'x-www-form-urlencoded') !== FALSE;
    }

    public static function getHttpMethod() {
        $method = strtoupper(filter_input(INPUT_SERVER, 'REQUEST_METHOD'));

        $overrideMethodHead = static::getRequest('X-HTTP-Method-Override');
        $overrideMethodBody = strtoupper(filter_input(INPUT_POST, '_method'));

        if ($method == 'POST' && in_array($overrideMethodHead, static::$httpMethods)) {
            $method = $overrideMethodHead;
        } elseif ($method == 'POST' && in_array($overrideMethodBody, static::$httpMethods)) {
            $method = $overrideMethodBody;
        }

        return $method;
    }

    /**
     * Verifica la existencia de la cabecera de la petici�n
     * @param string $header
     * @return mixed
     */
    public static function getRequest($header) {
        $headers = getallheaders();
        if ($header) {
            $response = \Helpers\Collection::get($header, $headers, FALSE);
        }
        return $response;
    }
    
    /**
     * Verifica la existencia de la cabecera de la respuesta
     * @param string $header
     * @return mixed
     */
    public static function getResponse($header) {
        $response = NULL;
        $pieces = [];
        $headers = headers_list();
        if ($header) {
            $headers = \Helpers\Collection::stristr($header, $headers, 0, 0);
            $pieces = explode(':', current($headers));
            array_shift($pieces);
        }
        if ($pieces) {
            $response = trim(implode(':',$pieces));
        }
        return $response;
    }

    public static function isGetHttpMethod() {
        return static::getHttpMethod() == 'GET';
    }

    public static function isPostHttpMethod() {
        return static::getHttpMethod() == 'POST';
    }

    public static function isPutHttpMethod() {
        return static::getHttpMethod() == 'PUT';
    }

    public static function isPatchHttpMethod() {
        return static::getHttpMethod() == 'PATCH';
    }

    public static function isDeleteHttpMethod() {
        return static::getHttpMethod() == 'DELETE';
    }

    public static function set($header, $value = NULL, $replace = TRUE, $code = NULL) {
        if (is_string($header)) {
            if (is_null($value)) {
                $string = $header;
            } else {
                $string = "$header: $value";
            }
            header($string, $replace, $code);
        } else {
            foreach ($header as $key => $value) {
                static::set($key, $value);
            }
        }
    }

}
