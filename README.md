# RESTful

Esta librería pretende estandarizar la manera como *Multinet* estructura sus proyectos RESTful

# Archivos

 - [OAuth](https://gitlab.com/workjob/Decameron/multinet/libraries/restful/tree/master/OAuth "OAuth")
	 - [Auth.php](https://gitlab.com/workjob/Decameron/multinet/libraries/restful/blob/master/OAuth/Auth.php "Auth.php") -- Clase responsable de la gestión de autenticación
	- [User.php](https://gitlab.com/workjob/Decameron/multinet/libraries/restful/blob/master/OAuth/User.php "User.php") -- Clase responsable de la gestión de usuarios en función el sistema de autenticación activo
 - [Header.php](https://gitlab.com/workjob/Decameron/multinet/libraries/restful/blob/master/Header.php "Header.php") -- Clase responsable de la gestión de cabeceras en peticiones y respuestas
- [Input.php](https://gitlab.com/workjob/Decameron/multinet/libraries/restful/blob/master/Input.php "Input.php") -- Clase responsable de la gestión de entradas en peticiones y respuestas
- [Request.php](https://gitlab.com/workjob/Decameron/multinet/libraries/restful/blob/master/Request.php "Request.php") -- Clase responsable de la petición en general
- [Response.php](https://gitlab.com/workjob/Decameron/multinet/libraries/restful/blob/master/Response.php "Response.php") -- Clase responsable de la respuesta en general
- [Restful.php](https://gitlab.com/workjob/Decameron/multinet/libraries/restful/blob/master/Restful.php "Restful.php") -- Rasgo de integración en proyectos

## Documentación

### Limitación de acceso a métodos

Si usted desea limitar el acceso a un método especifico solo debe utilizar el **doc-comment** `HttpMethod` definiendo uno o mas métodos HTTP separados por *pipeline* o incluso usando varias lineas 

#### Restricción única

```php
/**
 * @HttpMethod POST
 */
protected function pay() { ... }
```

#### Restricción múltiple

```php
/**
 * @HttpMethod POST|PUT
 */
protected function repay() { ... }

/**
 * @HttpMethod POST
 * @HttpMethod PUT
 */
protected function repay() { ... }
```
