<?php

namespace Restful;

/**
 * @author Neder Alfonso Fandi�o Andrade <neder.fandino@decameron.com>
 */
class Request {
    
    public $header;
    /**
     * @var Input
     */
    public $input;
    
    public function __construct() {
        $this->input = new Input();
    }
    
    /**
     * @param mixed $keys
     * @return mixed
     */
    public function input($keys = NULL) {
        $response = NULL;
        if (empty($keys)) {
            $response = $this->input->all();
        } elseif (is_scalar($keys)) {
            $response = $this->input->$keys;
        } else {
            $data = $this->input->all();
            $response = array_intersect($data, array_flip($keys));
        }
        return $response;
    }
}
