<?php

namespace Restful\OAuth;

use Exception;
use Restful\Input;

/**
 * @author Neder Alfonso Fandi�o Andrade <neder.fandino@decameron.com>
 */
class Auth {
    
    private $token;
    
    public function __construct() {
        $this->token = static::getToken();
        if (empty($this->token)) {
            throw new Exception('La consulta no provee un token valido');
        }
    }

    /**
     * @return string
     */
    protected static function getToken() {
        return Input::request('_token');
    }
    
    /**
     * @return object
     */
    public function user() {
        return User::get();
    }
}
