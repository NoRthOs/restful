<?php

namespace Restful;

use Object;

/**
 * @author Neder Alfonso Fandi�o Andrade <neder.fandino@decameron.com>
 */
class Input {

    /**
     * @return Object
     */
    public static function all() {
        $data = array_merge(static::getParams(), static::getBody());
        return static::parse($data);
    }
    
    /**
     * @param string $key
     * @return mixed
     */
    public static function request($key = NULL) {
        $data = static::all();
        if ($key) {
            $response = isset($data[$key]) ? $data[$key] : NULL;
        } else {
            $response = $data;
        }
        return $response;
    }

    /**
     * @param mixed $key
     * @return mixed
     */
    public static function get($key = NULL) {
        if (is_string($key)) {
            $response = filter_input(INPUT_GET, $key);
        } else {
            $response = static::getParams();
        }
        return static::parse($response);
    }

    /**
     * @return Object
     */
    public static function post() {
        $data = [];
        if (Header::isPostHttpMethod()) {
            $data = static::getBody();
        }
        return static::parse($data);
    }

    /**
     * @return Object
     */
    public static function put() {
        $data = [];
        if (Header::isPostHttpMethod()) {
            $data = static::getBody(TRUE);
        }
        return static::parse($data);
    }

    /**
     * @return Object
     */
    public static function delete() {
        $data = [];
        if (Header::isDeleteHttpMethod()) {
            $data = static::getBody(TRUE);
        }
        return static::parse($data);
    }

    /**
     * @return Object
     */
    public static function hateoas() {
        $response = NULL;
        $hateoas = Input::get('_hateoas');
        if (!is_null($hateoas)) {
            $pieces = array_filter(explode(',', $hateoas));
            $response = new Object();
            $response->setResponseForUndefinedDynamicAttribute(function() {
                return FALSE;
            });
            foreach ($pieces as $piece) {
                $response->$piece = TRUE;
            }
        }
        return $response;
    }
    
    /**
     * @param string $name
     * @return mixed
     */
    public function __get($name) {
        $data = static::all();
        return isset($data[$name]) ? $data[$name] : NULL;
    }

    /**
     * @return mixed
     */
    protected static function getPhpInput() {
        return file_get_contents('php://input');
    }

    /**
     * @return array
     */
    protected static function getParams() {
        return $_GET;
    }

    /**
     * @return array
     */
    protected static function getBody($force = FALSE) {
        $input = static::getPhpInput();        
        if ($_POST) {
            $input = $_POST;
        } elseif ($input && Header::isJsonContentTypeRequest()) {
            $input = json_decode($input, TRUE);
        } elseif ($force && $input && Header::isFormUrlencodedContentTypeRequest()) {
            $input = static::parseRawHttpRequestToFormUrlencoded($input);
        } elseif ($force && $input) {
            $input = static::parseRawHttpRequestToFormData($input);
        }

        if (empty($input)) {
            $input = [];
        } elseif (is_scalar($input)) {
            $input = ['raw' => $input];
        }

        return $input;
    }
    
    /**
     * @param array $input
     * @return array
     */
    protected static function parseRawHttpRequestToFormUrlencoded($input = NULL) {
        $matches = [];
        
        if (empty($input)) {
            $input = file_get_contents('php://input');
        }
        
        preg_match_all('/&*([^=]+)=([^&]*)/', $input, $matches);
        $response = array_combine($matches[1], $matches[2]);
        
        return $response;
    }
    
    /**
     * @param array $input
     * @return array
     */
    protected static function parseRawHttpRequestToFormData($input = NULL) {
        $response = $matches = [];
        
        if (empty($input)) {
            $input = file_get_contents('php://input');
        }
        
        preg_match('/boundary=(.*)$/', $_SERVER['CONTENT_TYPE'], $matches);
        $boundary = $matches[1];
        $blocks = preg_split("/-+$boundary/", $input);
        array_pop($blocks);
        
        foreach ($blocks as $block) {
            if (empty($block)) {
                continue;
            }
            if (strpos($block, 'application/octet-stream') !== FALSE) {
                preg_match("/name=\"([^\"]*)\".*stream[\n|\r]+([^\n\r].*)?$/s", $block, $matches);
            }
            else {
                preg_match('/name=\"([^\"]*)\"[\n|\r]+([^\n\r].*)?\r$/s', $block, $matches);
            }
            $response[$matches[1]] = $matches[2];
        }
        return $response;
    }
    
    /**
     * @param mixed $data
     * @return mixed
     */
    protected static function parse($data) {
        if (is_array($data)) {
            $response = new Object($data, TRUE);            
        } else {
            $response = $data;
        }
        return $response;
    }

}
